{
  "name": "semantic-release",
  "description": "Automate your versioning and release management with [semantic-release](https://github.com/semantic-release/semantic-release)",
  "template_path": "templates/gitlab-ci-semrel.yml",
  "kind": "publish",
  "prefix": "semrel",
  "job_prefix": "semantic-release",
  "is_component": true,
  "variables": [
    {
      "name": "SEMREL_IMAGE",
      "description": "The Docker image used to run semantic-release",
      "default": "registry.hub.docker.com/library/node:lts-slim"
    },
    {
      "name": "SEMREL_VERSION",
      "description": "The [semantic-release](https://www.npmjs.com/package/semantic-release) version to use",
      "default": "latest",
      "advanced": true
    },
    {
      "name": "SEMREL_EXEC_VERSION",
      "description": "The [@semantic-release/exec](https://www.npmjs.com/package/@semantic-release/exec) version to use",
      "default": "latest",
      "advanced": true
    },
    {
      "name": "GITLAB_TOKEN",
      "description": "A GitLab 'project access token' or 'personal access token' with `api`, `read_repository` and `write repository` scopes.",
      "mandatory": true,
      "secret": true
    },
    {
      "name": "SEMREL_CONFIG_DIR",
      "description": "directory containing your [semantic-release configuration](https://semantic-release.gitbook.io/semantic-release/usage/configuration#configuration-file)",
      "default": ".",
      "advanced": true
    },
    {
      "name": "SEMREL_TAG_FORMAT",
      "description": "For generated `.releaserc` file only. [tagFormat semantic-release option](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#tagformat)e. :warning: don't forget to double the `$` character so it is not interpreted by GitLab.",
      "default": "$${version}",
      "advanced": true
    },
    {
      "name": "SEMREL_REQUIRED_PLUGINS_FILE",
      "description": "Full path to `semrel-required-plugins.txt` file _(relative to `$CI_PROJECT_DIR`)_",
      "default": "semrel-required-plugins.txt",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "semantic-release",
      "name": "semantic-release",
      "description": "Performs a semantic release.",
      "disable_with": "SEMREL_RELEASE_DISABLED",
      "variables": [
        {
          "name": "SEMREL_CHANGELOG_ENABLED",
          "description": "Add the [@semantic-release/changelog](https://github.com/semantic-release/changelog) plugin which will commit a changelog file in the repository.",
          "type": "boolean"
        },
        {
          "name": "SEMREL_CHANGELOG_FILE",
          "description": "[changelogFile @semantic-release/changelog option](https://github.com/semantic-release/changelog#options).",
          "default": "CHANGELOG.md",
          "advanced": true
        },
        {
          "name": "SEMREL_CHANGELOG_TITLE",
          "description": "[changelogTitle @semantic-release/changelog option](https://github.com/semantic-release/changelog#options). You might want to use markdown format (for example `# MyApp Changelog`).",
          "advanced": true
        },
        {
          "name": "SEMREL_DRY_RUN",
          "description": "For generated `.releaserc` file only. Activate the [dryRun semantic-release option](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#dryrun) if present.",
          "type": "boolean",
          "advanced": true
        },
        {
          "name": "SEMREL_EXTRA_ARGS",
          "description": "[Extra options](https://semantic-release.gitbook.io/semantic-release/usage/configuration#options) to pass to semantic-release.",
          "advanced": true
        },
        {
          "name": "SEMREL_AUTO_RELEASE_ENABLED",
          "description": "When set the job start automatically. When not set (default), the job is manual.",
          "type": "boolean"
        },
        {
          "name": "SEMREL_BRANCHES_REF",
          "description": "Regular expression pattern matching branches from which releases should happen (should match your [semantic-release configuration](https://semantic-release.gitbook.io/semantic-release/usage/configuration#branches))",
          "default": "/^(master|main)$/",
          "advanced": true
        },
        {
          "name": "SEMREL_HOOKS_DIR",
          "description": "Hook scripts folder.",
          "default": ".",
          "advanced": true
        },
        {
          "name": "SEMREL_GPG_SIGNKEY",
          "description": "Path to the GPG signkey exported with `gpg --armor --export-secret-key`.",
          "secret": true,
          "advanced": true
        },
        {
          "name": "SEMREL_COMMIT_MESSAGE",
          "description": "[message @semantic-release/git option](https://github.com/semantic-release/git#message)",
          "advanced": true
        },
        {
          "name": "SEMREL_COMMIT_SPEC",
          "description": "Commit specification `preset` (possible values: `angular`, `atom`, `codemirror`, `ember`, `eslint`, `express`, `jquery`, `jshint`, `conventionalcommits`). The default is `angular`.",
          "values": ["angular","codemirror","conventionalcommits","ember","eslint","express","jquery","jshint"],
          "default": "angular",          
          "advanced": true
        }
      ]
    },
    {
      "id": "semantic-release-info",
      "name": "semantic-release-info",
      "description": "Run `semantic-release` dry run to save next release version information as dotenv artifact and make them available in next pipeline stages",
      "variables": [
        {
          "name": "SEMREL_INFO_ON",
          "description": "Define on which branch(es) the job shall be run",
          "type": "enum",
          "values": ["", "prod", "branches-ref", "protected", "all"],
          "mandatory": true
        }
      ]
    }
  ],
  "variants": [
    {
      "id": "vault",
      "name": "Vault",
      "description": "Retrieve secrets from a [Vault](https://www.vaultproject.io/) server",
      "template_path": "templates/gitlab-ci-semrel-vault.yml",
      "variables": [
        {
          "name": "TBC_VAULT_IMAGE",
          "description": "The [Vault Secrets Provider](https://gitlab.com/to-be-continuous/tools/vault-secrets-provider) image to use",
          "default": "registry.gitlab.com/to-be-continuous/tools/vault-secrets-provider:latest",
          "advanced": true
        },
        {
          "name": "VAULT_BASE_URL",
          "description": "The Vault server base API url",
          "mandatory": true
        },
        {
          "name": "VAULT_OIDC_AUD",
          "description": "The `aud` claim for the JWT",
          "default": "$CI_SERVER_URL"
        },
        {
          "name": "VAULT_ROLE_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) RoleID",
          "mandatory": true,
          "secret": true
        },
        {
          "name": "VAULT_SECRET_ID",
          "description": "The [AppRole](https://www.vaultproject.io/docs/auth/approle) SecretID",
          "mandatory": true,
          "secret": true
        }
      ]
    }
  ]
}
